package com.example.lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

data class Question( val textResId: Int, val answer: Boolean)

class MainActivity : AppCompatActivity() {
    lateinit var btnTrue : Button
    lateinit var btnFalse : Button
    lateinit var btnNext : Button
    lateinit var tvQuestion : TextView
    var currentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {

        btnTrue = findViewById(R.id.btnTrue)
        btnFalse = findViewById(R.id.btnFalse)
        btnNext = findViewById(R.id.btnNext)
        tvQuestion = findViewById(R.id.tvQuestion)


         val questionList = listOf(
            Question(R.string.q1, false),
            Question(R.string.q2, true),
            Question(R.string.q3, true))



         fun checkAnswer(userAnswer: Boolean){
            val correctAnswer = questionList[currentIndex].answer
            val messageResId = if (userAnswer == correctAnswer) {
                R.string.btntrue
            } else {
                R.string.btnfalse
            }
            Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
                .show()
        }
        btnTrue.setOnClickListener {
            checkAnswer(true)
        }
        btnFalse.setOnClickListener{
            checkAnswer(false)
        }
        btnNext.setOnClickListener {
            currentIndex ++
            tvQuestion.setText(questionList[currentIndex].textResId)
        }




        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}